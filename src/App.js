import './App.css';

const products = ['t-shirts', 'hats', 'shorts', 'jackets', 'pants', 'shoes'];

function App() {

  let renderCategories = (array) => (
    array.map((ele,index)=>{
      return <p key={index}>{ele}</p>
    })
    )

  return (
    <div className="App">
    <h1>These are the categories:</h1>
{
renderCategories(products)
}
    </div>
  )
}

export default App;
