import "@testing-library/jest-dom";
import { screen, render } from "@testing-library/react";
import Test from "../App.js";

beforeEach(() => render(<Test />));

describe("b1ex01: Display a list of product categories", () => {
  it("t-shirts should be in the document", () => {
    expect(screen.queryByText(/t-shirts/i)).toBeInTheDocument();
  });
  it("hats should be in the document", () => {
    expect(screen.queryByText(/hats/i)).toBeInTheDocument();
  });
  it("shorts should be in the document", () => {
    expect(screen.queryByText(/shorts/i)).toBeInTheDocument();
  });
  it("jackets should be in the document", () => {
    expect(screen.queryByText(/jackets/i)).toBeInTheDocument();
  });
  it("pants should be in the document", () => {
    expect(screen.queryByText(/pants/i)).toBeInTheDocument();
  });
  it("shoes should be in the document", () => {
    expect(screen.queryByText(/shoes/i)).toBeInTheDocument();
  });
});
